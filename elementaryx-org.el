;; Check out https://git.sr.ht/~ashton314/emacs-bedrock/tree/main/item/extras/org.el

;; Unified org / org-agenda / org-roam set up
;; See "Task management with org-roam" post series on https://d12frosted.io/
;; We assume a single ~/org-roam repository for org / org-agenda / org-roam.
;; Link your actual org-roam repository to ~/org-roam

;; TODO https://www.reddit.com/r/emacs/comments/135dhh5/code_snippet_to_switch_between_multiple/
;; https://www.badykov.com/emacs/split/

(use-package elementaryx-base)
(use-package elementaryx-org-minimal)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    ElementaryX Org Directory
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Org-Mode uses `org-directory' as a default location to look for Org
;; file. It is used when a capture template specifies a target file
;; that is not an absolute path. The path will then be interpreted
;; relative to ‘org-directory’.

;; Org-Agenda collects data from all agenda files, the files listed in
;; the variable `org-agenda-files'. If a directory is part of this
;; list, all files with the extension ‘.org’ in this directory are
;; part of the list.

;; Org-Roam crawls all files within `org-roam-directory', and maintains
;; a cache of all links and nodes. Org-Roam searches recursively
;; within org-roam-directory for notes. This variable needs to be set
;; before any calls to Org-roam functions.

;; In its default configuration, ElementaryX uses
;; `elementaryx-org-directory' to define `org-directory',
;; `org-agenda-files' and `org-roam-directory'. They end up all equal:
;; - `org-directory' and `org-agenda-files' are equal to
;;   `elementaryx-org-directory';
;; - `org-agenda-files' is more precisely a list composed of a unique
;;   element, `elementaryx-org-directory'.

;; In addition, `org-default-notes-file', the default target for
;; storing notes (used as a fall back file for org-capture.el, for
;; templates that do not specify a target file) is set up by ElementaryX
;; as `elementaryx-org-directory/refile.org'.

;; The idea is to have an elementary, flat overall Org-Mode set up
;; that does not need initial customization. Capturse simply go into
;; the `org-default-notes-file' file for later refiling. The notes
;; grow naturally with the non hiearchical Org-Roam structure. The
;; default note file is part of the Org-Roam structure. Captures
;; within the default note file (or within other Org-Roam notes) can
;; be refiled within Org-Roam notes. And the agenda is extracted from
;; those same Org-Roam notes (including the default note file) files.

;; By default `elementaryx-org-directory' is defined as "~/org-roam/":
(defcustom elementaryx-org-directory "~/org-roam/"
  "Base directory for Org-related files (captures, agenda, roam)."
  :type 'directory
  :group 'elementaryx)

;; This can be overriden by setting `elementaryx-org-directory' BEFORE
;; loading `elementaryx-org' package with:
;;
;; `(setq elementaryx-org-directory "~/org")'
;;
;; Or with `use-package' syntax:
;;
;; (use-package elementaryx-org
;;   :init
;;   (setq elementaryx-org-directory "~/org"))

;; It is also possible to override `org-directory', `org-agenda-files'
;; and `org-roam-directory' separately. In this case, it must happen
;; AFTER loading `elementaryx-org'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    ElementaryX Org Overall Design
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; The flat design of Org-Mode in ElementaryX allows for a simple workflow.

;; A note can be found, created or edited (`org-roam-node-find' or `C-c n
;; f') by name.

;; Captures (`org-capture' or `C-c c') end up in
;; `elementaryx-org-directory/refile.org' and may be refiled
;; (`org-refile' or `C-c C-w'), during or after capture, within any
;; note. It is also possible to capture literature notes
;; (`citar-org-roam-capture' or `C-c n c c').

;; The agenda may be extracted from those notes (`org-agenda' or `C-c a')

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Baseline Org-Mode
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package org
  ;; https://orgmode.org/worg/org-configs/org-customization-guide.html
  ;; https://orgmode.org/manual/Activation.html
  :bind (("C-c l" . org-store-link)
         ("C-c a" . org-agenda)
	 ("C-c A" . org-agenda-exit) ;; "Exit agenda, killing org-mode buffers loaded by the agenda"
	 ;; Also available by default from an agenda view by pressing 'x'
	 ("C-c c" . org-capture))
  :init
  (easy-menu-add-item nil
		      '("Tools") ; Insert withing the Edit menu
		      '("Org"
			["Store link"  org-store-link :help "Store a link to the current location. Can then be used in an org-mode document to insert it (with `M-x org-insert-link', `C-c C-l' or through the `Org / Hyperlinks' submenu."]
			["Agenda"  org-agenda :help "Dispatch org-agenda commands"]
			["Capture" org-capture :help "Capture something"]
			)
		      "Search Files (Grep)..." ; Insert before this item/submenu
		      )
  (easy-menu-add-item nil
		      '("Tools") ; menu into which we insert our item/submenu (submenu here)
		      "----"
		      "Search Files (Grep)..." ; Optional argument to position the added item before that one
		      )
  :custom
  (org-directory (file-truename  elementaryx-org-directory))
  (org-default-notes-file (concat org-directory "/refile.org"))
  (org-agenda-files (list org-directory))
  (org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "WAITING(w@/!)" "|" "DONE(d)" "CANCELLED(c@)")))
  (org-todo-keyword-faces (quote (("TODO" :foreground "red" :weight bold)
				  ("NEXT" :foreground "blue" :weight bold)
				  ("DONE" :foreground "forest green" :weight bold)
				  ("WAITING" :foreground "orange" :weight bold)
				  ("CANCELLED" :foreground "forest green" :weight bold))))
  ;; org-id is enabled in elementaryx-org-minimal; we define the interactive behaviour here:
  (org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id) ;; org-store-link uses custom id if it exists
  ;; Begin from: https://blog.aaronbieber.com/2017/03/19/organizing-notes-with-refile.html
  (org-refile-targets '((org-agenda-files :maxlevel . 5)))
  (org-refile-use-outline-path 'file)
  (org-outline-path-complete-in-steps nil)
  (org-refile-allow-creating-parent-nodes 'confirm)
  ;; End from: https://blog.aaronbieber.com/2017/03/19/organizing-notes-with-refile.html
  ;; TODO: to be further assessed and possibly refined
)

;; Add embark-consult-org-heading-map abd org-link-heading here
(use-package org
  :defer t
  :config
  ;; BEGIN https://gist.github.com/jdtsmith/8602d998116b953725218224b77b8766?permalink_comment_id=4356508#file-consult-org-heading-store-link-el
  (defun elementaryx/org-link-heading-here (cand)
    (when-let ((marker (get-text-property 0 'consult--candidate cand)))
      (save-excursion
	(with-current-buffer (marker-buffer marker)
	  (goto-char marker)
	  (org-store-link nil t)))
      (org-insert-all-links 1 "" " ")))
  (defvar-keymap embark-consult-org-heading-map
    :doc "Keymap for operating on org headings"
    :parent embark-general-map
    "l" 'elementaryx/org-link-heading-here)
  (add-to-list 'embark-keymap-alist '(consult-org-heading . embark-consult-org-heading-map))
  ;; make sure it quits to restore point; TODO: the following line is not working, to be checked why
  ;; (setf (alist-get 'my/org-link-heading-here embark-quit-after-action) t)
  )

(use-package consult-org
  :bind (("M-g a" . consult-org-agenda))
        (:map org-mode-map
              (("M-g h" . consult-org-heading))))

;; Ensures automatic loading of consult-org after org and consult
(use-package consult-org
  :after org consult)

;; TODO: Insert link from consult-org-headings with embark
;; https://gist.github.com/jdtsmith/8602d998116b953725218224b77b8766

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Org-Roam
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Org-Roam can be loaded only if elementaryx-org-directory exists and is a directory

(use-package org-roam
  :if (file-directory-p elementaryx-org-directory)
  ;; :after org ;; no need to declare the dependency, it's already declared within the package
  :config
  (setq org-roam-directory elementaryx-org-directory)
  (org-roam-db-autosync-mode)
    ;; Dedicated side window for backlinks
  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-side-window)
                 (side . right)
                 (window-width . 0.4)
                 (window-height . fit-window-to-buffer)))
  :init
  (which-key-add-key-based-replacements "C-c n" "notes")
  (which-key-add-key-based-replacements "C-c n r" "ref")
  (which-key-add-key-based-replacements "C-c n c" "cite") ;; see citar-org-roam in elementaryx-write
  ;; https://systemcrafters.net/build-a-second-brain-in-emacs/getting-started-with-org-roam/
  ;; https://org-roam.discourse.group/t/key-bindind-does-not-work/2380
  :bind
  ;; (:prefix-map notes-elementaryx-map
  ;; 	     :prefix-docstring "notes"
  ;; 	     :prefix "C-c n")
  ;; (:prefix-map notes-ref-elementaryx-map
  ;; 	     :prefix-docstring "notes ref"
  ;; 	     :prefix "C-c n r")
  (("C-c n B" . org-roam-buffer-toggle)
   ("C-c n f" . org-roam-node-find)
   ("C-c n r f" . org-roam-ref-find)
   ("C-c n c c" . org-roam-capture) ;; will be overridden within citar-org-roam
   (:map org-mode-map
         (("C-c n i" . org-roam-node-insert)
          ("C-c n o" . org-id-get-create)
          ("C-c n t" . org-roam-tag-add)
          ("C-c n a" . org-roam-alias-add)
	  ("C-c n r a" . org-roam-ref-add)
	  ("C-c n r R" . org-roam-ref-remove))))
  :init
  (easy-menu-add-item nil
		      '("Tools") ; Insert withing the Edit menu
		      '("Notes (Org-roam)"
			["Find or Create - by Name"  org-roam-node-find :help "Find and open an Org-roam node by its title or alias. Create it if no such note exists."]
			["Find - by Reference"  org-roam-ref-find :help "Find and open an Org-roam node that’s dedicated to a specific ref"]
			["Capture" org-roam-capture :help "Capture something. Launches an ‘org-capture’ process for a new or existing node."]
			["Toggle Back-Links Buffer" org-roam-buffer-toggle :help "Org-roam provides the Org-roam buffer: an interface to view relationships with other notes (backlinks, but also reference links, unlinked references etc.). This toggle launch an Org-roam buffer that tracks the node currently at point. This means that the content of the buffer changes as the point is moved, if necessary."]
			)
		      "Org" ; Insert before this item/submenu
		      )
  )

(use-package citar-org-roam
  :if (file-directory-p elementaryx-org-directory)
  ;; :after (citar org-roam) ;; no need to declare the dependency, it's already declared within the package!
  :preface
  ;; We define elementaryx/citar-org-roam-capture() as a simple wrapper
  ;; around org-roam-capture(). Defined within citar-org-roam
  ;; use-package scope, it will allow us autoload of citar-org-roam
  ;; when doing an org-roam-capture.
  (defun elementaryx/citar-org-roam-capture ()
    (interactive)
    (progn
      (citar-org-roam-mode) ;; force autoload of org-roam by calling a
			    ;; function of citar-org-roam
      (org-roam-capture)))
  :init
  (which-key-add-key-based-replacements "C-c n c" "citation")
  (which-key-add-key-based-replacements "C-c n c c" "citar-org-roam-capture") ;; better visual rendering than with "elementaryx/" prefix
  ;; org-roam-capture has been bound in org-roam use-package declaration above.
  ;; We re-rebind org-roam-capture here so that org-roam-capture auto-loads citar-org-roam.
  :bind (("C-c n c c" . elementaryx/citar-org-roam-capture)
	 ("C-c n c C" . citar-org-roam-cited)
	 (:map org-mode-map
               (("C-c n c a" . citar-org-roam-ref-add)
		("C-c n r c" . citar-org-roam-ref-add))))
  :config
  (citar-org-roam-mode)
  (setq citar-org-roam-note-title-template "${author} - ${title}\n#+filetags: ${tags}")
  (add-to-list 'org-roam-capture-templates
               '("n" "literature note" plain
		 "%?"
		 :target
		 (file+head
		  "%(expand-file-name (or citar-org-roam-subdir \"\") org-roam-directory)/${citar-citekey}.org"
		  "#+title: ${citar-citekey} (${citar-date}). ${note-title}.\n#+created: %U\n#+last_modified: %U\n\n")
		 :unnarrowed t))
  ;; use this “literature note” ("n") template for new bibliographic notes:
  (setq citar-org-roam-capture-template-key "n"))

;; Ensures citar loading automatically loads citar-org-roam
(use-package citar-org-roam
  :if (file-directory-p elementaryx-org-directory)
  :after citar
  :config (citar-org-roam-mode))

;; https://github.com/jgru/consult-org-roam
(use-package consult-org-roam
  :if (file-directory-p elementaryx-org-directory)
  :init
  :custom
  ;; Use `ripgrep' for searching with `consult-org-roam-search'
  (consult-org-roam-grep-func #'consult-ripgrep)
  ;; Configure a custom narrow key for `consult-buffer'
  ;; (consult-org-roam-buffer-narrow-key ?r) ;; commented as we keep the 'n' ("notes") org-roam default narrow key
  ;; Display org-roam buffers right after non-org-roam buffers
  ;; in consult-buffer (and not down at the bottom)
  (consult-org-roam-buffer-after-buffers t) ;; TODO: not working properly yet; shall work properly after upstream https://github.com/jgru/consult-org-roam/commit/8e5b60a61eee9d0582afd309bc4e70ca3b1054cb fix has been integrated into guix
  :config
  ;; We define rewrap consult-line, consult-grep, consult-ripgrep,
  ;; consult-git-grep to start with current symbol at point
  ;; See https://github.com/minad/consult/wiki#start-consult-line-search-with-symbol-at-point
  (defun elementaryx-consult-org-roam-search ()
    "Search for a matching line forward."
    (interactive)
    (consult-org-roam-search (thing-at-point 'symbol)))
  ;; Eventually suppress previewing for certain functions
  (consult-customize
   consult-org-roam-forward-links
   :preview-key "M-.")
  :bind
  ;; Define some convenient keybindings as an addition
  (("C-c n F" . consult-org-roam-file-find)
   ("C-c n /" . consult-org-roam-search) ;; [non standard, rather than "C-c n s"]
   ("M-s n" . consult-org-roam-search) ;; [non standard]
   ("M-s N" . elementaryx-consult-org-roam-search)) ;; [non standard]
  (:map org-mode-map
	(("C-c n b" . consult-org-roam-backlinks)
	 ("C-c n l" . consult-org-roam-forward-links))))

;; Ensures org-roam loading automatically loads consult-org-roam
(use-package consult-org-roam
  :if (file-directory-p elementaryx-org-directory)
  :after  org-roam
  :config (consult-org-roam-mode))

;; TODO: Possibly use consult-notes rather than consult-org-roam.
;; Consult-notes may have multiple back-ends including org-roam
;; https://github.com/mclear-tools/consult-notes
;; https://github.com/mclear-tools/consult-notes?tab=readme-ov-file#related-packages

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Other features
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org-babel asynchronous executions with ob-async ;;
;; https://github.com/astahlman/ob-async           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Asynchronous src_block execution for org-babel

;; Normally, when you execute a code block in Org mode, Emacs will
;; block until the code finishes executing. ob-async eliminates this
;; blocking behavior by running the code in the background.

(use-package ob-async
  :after org
  :config
  (setq ob-async-no-async-languages-alist '("ipython"))) ;; Note that we cannot write it in a :custom block as the variable is not initialized by the package

;; Once installed, it works automatically for any Org Babel code
;; block, provided the language is supported. No additional
;; configuration is required.

;; When you execute a block of code that would normally block Emacs.
;; Sessions (`:session') in vanilla emacs alternatively allow
;; asynchronous executions with `:async' header arg. But most non
;; session org-babel blocks do not have such support. Python
;; as such as support with `ipython' and in this case we let
;; `ipython' itself handle asynchronism; hence the customization
;; of `ob-async-no-async-languages-alist', as suggested there:
;; https://github.com/astahlman/ob-async?tab=readme-ov-file#ob-async-no-async-languages-alist

;; ob-async runs it in the background. Here's an example with a shell:

;; #+begin_src sh :async
;; sleep 10
;; echo "This will run in the background"
;; #+end_src

;; And another one with python:

;; #+begin_src python :async
;; import time
;; time.sleep(5)
;; print("Async execution complete")
;; #+end_src

;; Debugging: You can debug issues by examining the messages buffer
;; (*Messages*) or running the async-shell-command manually for deeper
;; insight into what happened during execution.

;; Note: if you use Julia, please consider reading
;; https://github.com/astahlman/ob-async?tab=readme-ov-file#ob-async-pre-execute-src-block-hook

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org-babel-tangle: jump-to-src back and forth ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Suggestion by Gilles Marait. The original source code is from Stas
;; as shared on
;; https://emacs.stackexchange.com/questions/50649/jumping-from-a-source-block-to-the-tangled-file

;; There exists an `org-babel-tangle-jump-to-org' function. However,
;; this function is not bound to a key. In addition there is no
;; converse operation to jump to src.

;; We do so with `elementaryx-org-babel-tangle-jump-to-src'. In
;; addition, we bind both to "C-c C-'".

;; We also bind "C-c C-v t" to org-babel-detangle to have a symmetric
;; behaviour for tangling / detangling.

;; Use ":comments link" or ":comments both" for effective use.
;; https://orgmode.org/manual/Extracting-Source-Code.html

(use-package org
  :bind (("C-c C-'" . org-babel-tangle-jump-to-org)  ;; Global binding
	 ("C-c C-v t" . org-babel-detangle)
         :map org-mode-map
         ("C-c C-'" . elementaryx-org-babel-tangle-jump-to-src)) ;; Binding within org-mode
  ; :custom
  ;; This is optional but might look better than defaults
  ;; We will decide whether to set it by default later.
  ; (org-babel-tangle-comment-format-beg "┌── BEGIN TANGLED BLOCK ──┐  [[%link][%source-name]]")
  ; (org-babel-tangle-comment-format-end "└──  END TANGLED BLOCK  ──┘   %source-name ends here")
  :config

  (defun elementaryx-org-babel-tangle-jump-to-src ()
    "The opposite of `org-babel-tangle-jump-to-org'. Jumps at tangled code from org src block."
    (interactive)
    (if (org-in-src-block-p)
	(let* ((header (car (org-babel-tangle-single-block 1 'only-this-block)))
               (tangle (car header))
               (lang (caadr header))
               (buffer (nth 2 (cadr header)))
               (org-id (nth 3 (cadr header)))
               (source-name (nth 4 (cadr header)))
               (search-comment (org-fill-template
				org-babel-tangle-comment-format-beg
				`(("link" . ,org-id) ("source-name" . ,source-name))))
               (file (expand-file-name
                      (org-babel-effective-tangled-filename buffer lang tangle))))
          (if (not (file-exists-p file))
              (message "File does not exist. 'org-babel-tangle' first to create file.")
            (find-file file)
            (beginning-of-buffer)
            (search-forward search-comment)))
      (message "Cannot jump to tangled file because point is not at org src block."))))

;;;;;;;;;;
;; TODO ;;
;;;;;;;;;;

;;; Links

;; Video on links: https://www.youtube.com/watch?v=eoIfLS4zMa8 : internal links (headline, #+NAME, CUSTOM_ID, <<<target>>>

;; https://www.reddit.com/r/emacs/comments/une2d8/orgmode_links_in_2022_implementation_packages/

;;;;;;;;;;;;;;;;;;;;
;; TODO: handling links in the export/publish side: to be handled by compose-publish
;;;;;;;;;;;;;;;;;;;;

;; https://emacs.stackexchange.com/questions/44665/is-there-a-way-to-suggest-label-names-in-org-mode-latex-export
;; See org-latex-prefer-user-labels variable (defined in ‘ox-latex.el’).
;; See org-html-prefer-user-labels
;; In the html case, CUSTOM_ID are always used as a reference, independently of org-html-prefer-user-labels
;; If we rely on CUSTOM_ID, we therefore not need to use org-html-prefer-user-labels

(provide 'elementaryx-org)
